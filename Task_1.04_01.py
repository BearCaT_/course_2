#=========================================================================================================
#                  FUNCTION_LIB.PY     
#=========================================================================================================
def create(namespace: str, parent: str, all_namespaces: dict) -> None:
    """
    Функция создает новое пространство имен с именем <namespace> внутри пространства <parent>.

    Параметры:
        namespace: Имя нового пространства имен.
        parent: Имя родительского пространства имен.
        all_namespaces: Словарь со всеми областями имен формата {namespace:[parent, var1, ..., varN]}

    Возвращаемое значение:
        return: None
    """
    all_namespaces.update({namespace: [parent]})
    return None


def add(namespace: str, var: str, all_namespaces: dict) -> None:
    """
    Функция добавляет в указанное пространство имен с именем <namespace>, переменную var.

    Параметры:
        namespace: Имя пространства имен.
        var: Имя переменной для добавления.
        all_namespaces: Словарь со всеми областями имен формата {namespace:[parent, var1, ..., varN]}

    Возвращаемое значение:
        return: None
    """
    if all_namespaces.get(namespace) is None:
        all_namespaces.update({namespace: [None]})
    all_namespaces[namespace].append(var)
    return None


def get(namespace: str, var: str, all_namespaces: dict) -> str:
    """
    Функция добавляет в указанное пространство имен с именем <namespace>, переменную var.

    Параметры:
        namespace: Имя пространства имен.
        var: Имя переменной для поиска.
        all_namespaces: Словарь со всеми областями имен формата {namespace:[parent, var1, ..., varN]}

    Возвращаемое значение:
        return: Имя пространства, из которого взята переменная var, или None, если такого пространства не существует
    """
    if all_namespaces.get(namespace) is None:
        return None
    elif var in all_namespaces[namespace]:
        return namespace
    elif all_namespaces[namespace][0] is None:
        return None
    else:
        return get(all_namespaces[namespace][0], var, all_namespaces)


#=========================================================================================================
#                  MAIN.PY     
#=========================================================================================================
"""
Реализуйте программу, которая будет эмулировать работу с пространствами имен. Необходимо реализовать
поддержку создания пространств имен и добавление в них переменных. В данной задаче у каждого пространства
имен есть уникальный текстовый идентификатор – его имя. Вашей программе на вход подаются следующие запросы:
1. create <namespace> <parent> –  создать новое пространство имен с именем <namespace> внутри пространства <parent>
2. add <namespace> <var> – добавить в пространство <namespace> переменную <var>
3. get <namespace> <var> – получить имя пространства, из которого будет взята переменная <var> при запросе из
                           пространства <namespace>, или None, если такого пространства не существует

Формат входных данных:
В первой строке дано число n (1 ≤ n ≤ 100) – число запросов.
В каждой из следующих n строк дано по одному запросу.
Запросы выполняются в порядке, в котором они даны во входных данных.
Имена пространства имен и имена переменных представляют из себя строки
длины не более 10, состоящие из строчных латинских букв.

Формат выходных данных:
Для каждого запроса get выведите в отдельной строке его результат.
"""
from function_lib import create, add, get
# число запросов
request_amount = int(input())
# все пространства имен
all_namespaces = dict()
# результат выполнения списка команд
result = []
for i in range(request_amount):
    # запрос строки с командой (create/add/get) и аргументами
    command, namespace, argument = input().split()
    # исполнение известных команд
    if command == 'create':
        # создать новое пространство имен с именем <namespace> внутри пространства <parent>
        create(namespace, argument, all_namespaces)
    elif command == 'add':
        # добавить в пространство <namespace> переменную <var>
        add(namespace, argument, all_namespaces)
    elif command == 'get':
        # получить имя пространства, из которого будет взята переменная <var> при запросе из пространства
        # <namespace>, или None, если такого пространства не существует
        result.append(get(namespace, argument, all_namespaces))
# вывод результата
for line in result:
    print(line)
