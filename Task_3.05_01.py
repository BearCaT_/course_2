"""
В этой задаче вам необходимо воспользоваться API сайта numbersapi.com
Вам дается набор чисел. Для каждого из чисел необходимо узнать, существует
ли интересный математический факт об этом числе.
Для каждого числа выведите Interesting, если для числа существует интересный факт, и Boring иначе.
Выводите информацию об интересности чисел в таком же порядке, в каком следуют числа во входном файле.
Пример запроса к интересному числу:
http://numbersapi.com/31/math?json=true
Пример запроса к скучному числу:
http://numbersapi.com/999/math?json=true
Пример входного файла:
31
999
1024
502
Пример выходного файла:
Interesting
Boring
Interesting
Boring
"""


def number_check(number: int) -> str:
    """
     Функция проверяет число на наличие интересных фактов о нем.

     Параметры:
         number: Число для проверки.

     Возвращаемое значение:
         return: 'Interesting', если для числа number существует интересный факт, и 'Boring' иначе
     """

    import requests
    if requests.get('http://numbersapi.com/{}/math'.format(number), params={"default": "Boring"}).text == 'Boring':
        return 'Boring'
    else:
        return 'Interesting'


def main() -> None:
    # Тестовая функция.
    import os
    # файл с входными данными
    i_file_name: str = os.path.join('c:', os.sep, 'work', 'download', 'dataset_24476_3.txt')
    # чтение чисел из файла
    with open(i_file_name) as i_file:
        # для каждого числа из файла
        for number in i_file:
            # вывод 'Interesting', если для числа существует интересный факт, и 'Boring' иначе
            print(number_check(int(number)))


if __name__ == "__main__":
    main()
