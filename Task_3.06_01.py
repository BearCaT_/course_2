"""
Вам дано описание пирамиды из кубиков в формате XML.
Кубики могут быть трех цветов: красный (red), зеленый (green) и синий (blue).
Для каждого кубика известны его цвет, и известны кубики, расположенные прямо под ним.
Пример:
<cube color="blue">
  <cube color="red">
    <cube color="green">
    </cube>
  </cube>
  <cube color="red">
  </cube>
</cube>
Введем понятие ценности для кубиков. Самый верхний кубик, соответствующий корню XML документа имеет ценность 1.
Кубики, расположенные прямо под ним, имеют ценность 2. Кубики, расположенные прямо под нижележащими кубиками,
имеют ценность 3. И т. д.
Ценность цвета равна сумме ценностей всех кубиков этого цвета.
Выведите через пробел три числа: ценности красного, зеленого и синего цветов.
Sample Input:
<cube color="blue"><cube color="red"><cube color="green"></cube></cube><cube color="red"></cube></cube>
Sample Output:
4 3 1
"""


class ColorCounter:
    """
    Класс - счеткчик веса цветов в  XML файле. Методы объекта класса используются XMLParser.feed

    Атрибуты
    ----------
    red : int
        Суммарный вес XML тегов с атрибутом 'red'
    green : int
        Суммарный вес XML тегов с атрибутом 'green'
    blue : int
        Суммарный вес XML тегов с атрибутом 'blue'
    depth: int
        Глубина вложенности XML файла на текущем считываемом методом feed тэгом.
        На самом деле это так же 'вес' цвета.

    Методы
    -------
    start(self, tag, attrib) -> None
        Вызывается методом XMLParser.feed для каждого нового открывающего XML тэга
    end(self, tag) -> None:
        Вызывается методом XMLParser.feed для каждого нового закрывающего XML тэга
    close(self) -> tuple:
        Вызывается методом XMLParser.feed после того как все входные данные были распарсены
    """
    def __init__(self):
        """
        Конструктор
        """
        self.red = 0
        self.green = 0
        self.blue = 0
        self.depth = 0

    def start(self, tag, attrib) -> None:
        """
        Параметры:
            tag: Имя XML тэга
            attrib: Атрибуты XML тэга в формате {имя: значение, ...}
        """
        self.depth += 1
        if attrib['color'] == 'red':
            self.red += self.depth
        elif attrib['color'] == 'green':
            self.green += self.depth
        elif attrib['color'] == 'blue':
            self.blue += self.depth

    def end(self, tag) -> None:
        """
        Параметры:
            tag: Имя XML тэга
        """
        self.depth -= 1

    def close(self) -> tuple:
        """
        Возвращаемое значение:
            return: Котреж со значениями весов цветов
        """
        return self.red, self.green, self.blue


def main() -> None:
    # Тестовая функция.
    from xml.etree.ElementTree import XMLParser
    # создание объекта XMLParser - для разбора XML контента
    parser = XMLParser(target=ColorCounter())
    # 'скармливаем' входной XML текст - парсеру
    parser.feed(str(input()))
    # выводим результаты подсчета
    print(*parser.close())


if __name__ == "__main__":
    main()
