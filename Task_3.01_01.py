"""
Вашей программе на вход подаются три строки s, a, b, состоящие из строчных латинских букв.
За одну операцию вы можете заменить все вхождения строки a в строку s на строку b.
Например, s = "abab", a = "ab", b = "ba", тогда после выполнения одной операции строка s перейдет
в строку "baba", после выполнения двух и операций – в строку "bbaa", и дальнейшие операции не будут
изменять строку s.
Необходимо узнать, после какого минимального количества операций в строке s не останется вхождений
строки a. Если операций потребуется более 1000, выведите Impossible.
Выведите одно число – минимальное число операций, после применения которых в строке s не останется
вхождений строки a, или Impossible, если операций потребуется более 1000.
"""


def get_replace_count(s: str, a: str, b: str, replace_count: int = 0) -> str:
    """
    Рекурсивная функция. Определяет - минимальное количество итераций замен строки 'a'
    на строку 'b' в строке 's' до тех пор пока в строке 's' не останется вхождений строки 'а'.
    Если операций потребуется более 1000, выводится слово - Impossible.

    Параметры:
        s: Строка в которой производится замена символов.
        a: Строка символов - для поиска и дальнейшей замены в строке 's'.
        b: Строка символов - на которую меняют найденную строку 'а'.
        replace_count: Количество операций, после применения которых в строке 's'
                       не останется вхождений строки 'a'. Стартовое значение по умолчанию = 0

    Возвращаемое значение:
        return: Минимальное количество операций, после применения которых в строке 's'
                не останется вхождений строки 'a' или слово - Impossible, если таких
                операций > 1000.
    """
    count = s.count(a)
    if count == 0:
        return str(replace_count)
    else:
        replace_count += 1
        if replace_count > 1000:
            return 'Impossible'
        else:
            s = s.replace(a, b)
            return get_replace_count(s, a, b, replace_count)


def main() -> None:
    # Тестовая функция
    from sys import setrecursionlimit
    # устанавливает чуть большую, максимальную глубину стека интерпретатора
    # что бы избежать ислючения если рекурсия будет глубиной в 1000
    setrecursionlimit(1050)
    # считываем с консоли 3 входные строки
    s, a, b = [str(input()), str(input()), str(input())]
    # высчитываем и выводим Минимадьное количество операций - либо слово 'Impossible'
    print(get_replace_count(s, a, b))


if __name__ == "__main__":
    main()
