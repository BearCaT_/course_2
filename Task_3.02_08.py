"""
Вам дана последовательность строк.
В каждой строке поменяйте местами две первых буквы в каждом слове, состоящем хотя бы из двух букв.
Буквой считается символ из группы \w.
"""


def letter_swap(match_obj):
    return match_obj.group(2) + match_obj.group(1)


def main() -> None:
    # Тестовая функция.
    import re
    import sys
    # получаем генератор - для считывание последовательности строк с консоли
    reader = (line.rstrip() for line in sys.stdin)
    # для каждой новой считаной с консоли строки - замена двух первых букв в каждом
    # слове - друг с другом
    for line in reader:
        # выводим строку в консоль
        print(re.sub(r'\b(\w)(\w)', letter_swap, line))


if __name__ == "__main__":
    main()
