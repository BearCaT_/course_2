"""
Вам дана частичная выборка из датасета зафиксированных преступлений,
совершенных в городе Чикаго с 2001 года по настоящее время.
Одним из атрибутов преступления является его тип – Primary Type.
Вам необходимо узнать тип преступления, которое было зафиксировано
максимальное число раз в 2015 году.
"""


def main() -> None:
    # Тестовая функция.
    import csv
    import os
    # файл с входными данными
    i_file_name: str = os.path.join('c:', os.sep, 'work', 'download', 'crimes.csv')
    # словарь с результатами подсчета количества типов переступления
    # формат: {'тип приступления': количество, ....}
    result_dictionary = dict()
    # чтение данных из CSV файла i_file_name
    with open(i_file_name) as i_file:
        index = 0
        for line in csv.reader(i_file):
            index += 1
            # если считываемая строка - первая
            if index == 1:
                try:
                    # определяем индекс "колонки" по имени 'Primary Type'
                    i = line.index('Primary Type')
                # обработка исключения на тот случай если имя колонки не
                # будет найдено в первой строке файла
                except ValueError:
                    # выход их цикла
                    break
            # если считываемая строка НЕ первая
            else:
                # если ключа с названием типа преступления в словаре result_dictionary - нет
                if result_dictionary.get(line[i]) is None:
                    # добавляем в словарь пару {ключ: 1}
                    result_dictionary.update({line[i]: 1})
                # если ключ есть в словаре result_dictionary
                else:
                    # увеличиваем значение в словаре result_dictionary по ключу на +1
                    result_dictionary.update({line[i]: result_dictionary.get(line[i]) + 1})
    # определяем максимальное число среди значений словаря и выводим его ключ
    max_kv = max(list(result_dictionary.items()), key=lambda z: z[1])
    print(max_kv[0])


if __name__ == "__main__":
    main()
