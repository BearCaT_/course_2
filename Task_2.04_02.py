"""
Вам дана в архиве (ссылка) файловая структура, состоящая из директорий и файлов.
Вам необходимо распаковать этот архив, и затем найти в данной в файловой структуре
все директории, в которых есть хотя бы один файл с расширением ".py".
Ответом на данную задачу будет являться файл со списком таких директорий,
отсортированных в лексикографическом порядке.
"""


def main() -> None:
    # Тестовая функция
    import os
    # путь к файлу с выходными данными
    o_file_name: str = os.path.join("/", "Users", "bearcat", "Downloads", "output.txt")
    # стартовая директория файловой структуры "main"
    start_dir: str = os.path.join("/", "Users", "bearcat", "Downloads", "main")
    # открытие файла с именем o_file_name на запись
    with open(o_file_name, 'w') as o_file:
        # создание списка с директориями файловой структуры "main",
        # в которых содержится хотя бы один файл *.py
        result_list = [current_dir.replace('/Users/bearcat/Downloads/', '') + '\n'
                       for current_dir, _, child_files in os.walk(start_dir)
                       if child_files and '.py' in ' '.join(child_files)]
        # сортировка полученого списка
        result_list.sort()
        # запись содержимого списка в открытый до этого - выходной файл
        o_file.writelines(result_list)


if __name__ == "__main__":
    main()
