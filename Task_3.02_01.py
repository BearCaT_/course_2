"""
Вам дана последовательность строк.
Выведите строки, содержащие "cat" в качестве подстроки хотя бы два раза.
Примечание:
Считать все строки по одной из стандартного потока ввода вы можете, например, так
import sys
for line in sys.stdin:
    line = line.rstrip()
    # process line
"""


def main() -> None:
    # Тестовая функция.
    import re
    import sys
    # получаем генератор - для считывание последовательности строк с консоли
    reader = (line.rstrip() for line in sys.stdin)
    # проверка для каждой новой считаной с консоли строки
    for line in reader:
        # если в строке содержится 'cat' > 2 раз
        result = re.findall(r'cat', line)
        if len(result) >= 2:
            # выводим строку в консоль
            print(line)


if __name__ == "__main__":
    main()
