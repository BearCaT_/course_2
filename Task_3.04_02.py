"""
Вам дано описание наследования классов в формате JSON.
Описание представляет из себя массив JSON-объектов, которые соответствуют классам.
У каждого JSON-объекта есть поле name, которое содержит имя класса, и поле parents,
которое содержит список имен прямых предков.
Пример:
[{"name": "A", "parents": []}, {"name": "B", "parents": ["A", "C"]}, {"name": "C", "parents": ["A"]}]
Гарантируется, что никакой класс не наследуется от себя явно или косвенно, и что
никакой класс не наследуется явно от одного класса более одного раза.
Для каждого класса вычислите предком скольких классов он является и выведите эту
информацию в следующем формате.
<имя класса> : <количество потомков>
Выводить классы следует в лексикографическом порядке.
"""


def get_children_set(class_name: str, json_data: list) -> set:
    """
     Рекурсивная функция, которая ищет для класса все его дочерние классы в данных формата JSON.

     Параметры:
         class_name: Имя класса для которого будем искать всех его потомков.
         json_data: Данные формата JSON, вида: [{"name": "..", "parents": [.., .., ...]}, {...}, ...]

     Возвращаемое значение:
         return: множетсво классов родителем которых является класс с именем class_name
     """
    result = set()
    for inheritance_set in json_data:
        if class_name in inheritance_set['parents']:
            result = result.union({inheritance_set['name']}, get_children_set(inheritance_set['name'], json_data))
    return result


def main() -> None:
    # Тестовая функция.
    import json
    # данные с консоли в формате JSON
    json_data = json.loads(input())
    # множество всех имен классов из считанных данных
    class_set = set()
    # список имен классов с количеством наследников формата: [[класс, количество], [.., ..], ...]
    # в лексикографическом порядке
    result_list = list()
    # заполняем class_set
    for item in json_data:
        class_set.add(item['name'])
        class_set.update(item['parents'])
    # для каждого класса из class_set, находим количество дочерних классов(включая самого себя)
    # заполняем result_list
    for cl in class_set:
        result_list.append([cl, len(get_children_set(cl, json_data)) + 1])
    # сортировка списка результатов в лексикографическом порядке
    result_list.sort(key=lambda i: i[0])
    # вывод списка результатов
    for lst in result_list:
        print(lst[0], ':', str(lst[1]), end='\n')


if __name__ == "__main__":
    main()
