#=========================================================================================================
#                  FUNCTION_LIB.PY     
#=========================================================================================================
def is_class_parent(parent: str, child: str, class_hierarchy: list, class_set: set) -> str:
    """
    Данная функция для каждого запроса (в виде пары классов parent/child) выводит слово "Yes",
    если parent является предком child, и ничего не возвращает (возвращает None), если не является.

    Параметры:
        parent: Имя класса1 - предполагаемый родитель.
        child: Имя класса2 - предполагаемый потомок.
        class_hierarchy: Список с иерархией наследования классов - формата [[parent1, child1], [parent2, child2], ...]
        class_set: Множетсво всех классов - формата (class1, class2, ...)

    Возвращаемое значение:
        return: "Yes", если parent является предком child, и None, если не является
    """
    if parent == child and parent in class_set:
        return 'Yes'
    if parent not in class_set or child not in class_set:
        return None
    for class_couple in class_hierarchy:
        if parent == class_couple[0] and child == class_couple[1]:
            return 'Yes'
        elif parent == class_couple[0]:
            if is_class_parent(class_couple[1], child, class_hierarchy, class_set) == 'Yes':
                return 'Yes'


def get_all_class_info(class_hierarchy: list, class_set: set) -> None:
    """
    Функция запрашивает и формирует информацию о классах и их иерархии наследования. Формат запроса данных:
    в первой строке входных данных содержится целое число n - число классов, в следующих n строках содержится
    описание наследования классов в формате "child : parent1 parent2 ...". Класс может ни от кого не наследоваться и
    класс не наследуется сам от себя (прямо или косвенно), а так же класс не наследуется явно от одного класса
    более одного раза.

    Параметры:
        class_hierarchy: Список с иерархией наследования классов - формата [[parent1, child1], [parent2, child2], ...]
        class_set: Множетсво всех классов - формата (class1, class2, ...)
    """
    # число классов с описанием наследования
    class_line_count = int(input())
    # запрос информации о классах и их наследовании
    while class_line_count != 0:
        class_line_count -= 1
        # строка с названием класса и описанием наследования
        class_line = str(input())
        # если класс имеет хотя бы одного родителя
        if ':' in class_line:
            # класс и его родители
            child_class, parent_classes = class_line.split(':')
            child_class = child_class.strip()
            # добавляем класс в множество всех классов
            class_set.add(child_class)
            parent_classes = parent_classes.strip()
            for parent in parent_classes.split():
                # добавляем класс в множество всех классов
                class_set.add(parent)
                # добавляем в список иерархии пару [родительский класс;дочерний класс]
                class_hierarchy.append([parent, child_class])
        # если нет ни одного родителя
        else:
            # добавляем класс в множество всех классов
            class_set.add(class_line.strip())
    return None


#=========================================================================================================
#                  MAIN.PY     
#=========================================================================================================
"""
Вам необходимо отвечать на запросы, является ли один класс предком другого класса
Важное примечание:
Создавать классы не требуется.
Мы просим вас промоделировать этот процесс, и понять существует ли путь от одного класса до другого.

Формат входных данных
В первой строке входных данных содержится целое число n - число классов.
В следующих n строках содержится описание наследования классов. В i-й строке указано от каких классов наследуется
i-й класс. Обратите внимание, что класс может ни от кого не наследоваться. Гарантируется, что класс не наследуется
сам от себя (прямо или косвенно), что класс не наследуется явно от одного класса более одного раза.
В следующей строке содержится число q - количество запросов.
В следующих q строках содержится описание запросов в формате <имя класса 1> <имя класса 2>.
Имя класса – строка, состоящая из символов латинского алфавита, длины не более 50.

Формат выходных данных
Для каждого запроса выведите в отдельной строке слово "Yes", если класс 1 является предком класса 2, и "No",
если не является.

Sample Input:
4
A
B : A
C : A
D : B C
4
A B
B D
C D
D A
Sample Output:
Yes
Yes
Yes
No
"""
from function_lib import is_class_parent
from function_lib import get_all_class_info
# описание иерархии наследования классов
class_hierarchy = []
# множество всех классов
class_set = set()
# запрос данных о классах и информации о их наследовании
get_all_class_info(class_hierarchy, class_set)
# количество запросов
request_line_count = int(input())
# результаты проверки
check_result = list()
# запрос данных для проверки
while request_line_count != 0:
    request_line_count -= 1
    parent, child = input().split()
    # проверка и вывод "Yes", если parent является предком child, и "No", если не является
    if is_class_parent(parent, child, class_hierarchy, class_set) is None:
        check_result.append('No')
    else:
        check_result.append('Yes')
# вывод результатов проверки
for res in check_result:
    print(res)
