"""
Вам дается текстовый файл, содержащий некоторое количество непустых строк.
На основе него сгенерируйте новый текстовый файл, содержащий те же строки в обратном порядке.
"""


def main() -> None:
    # Тестовая функция.
    import os
    # файл с входными данными
    i_file_name: str = os.path.join("/", "Users", "bearcat", "Downloads", "dataset_24465_4.txt")
    # файл с выходными данными
    o_file_name: str = os.path.join("/", "Users", "bearcat", "Downloads", "output.txt")
    # чтение текста из файла i_file_name и запись в
    with open(i_file_name) as i_file, open(o_file_name, 'w') as o_file:
        # содержимое входного файла в виде списка
        file_content = [text_line for text_line in i_file]
        # переворачиваем список 'наоборот'
        file_content.reverse()
        # вывод склейки перевернутого списка в файл
        o_file.write(''.join(file_content))


if __name__ == "__main__":
    main()
